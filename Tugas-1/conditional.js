// 1) If-else

var nama1 = ""
var peran1 = ""

if (nama1 == "") {
    console.log("Nama harus diisi!")
}

console.log("\n")
var nama2 = "Amber"
var peran2 = ""

if (peran2 == "") {
    console.log("Hello, " + nama2 + ". Pilih peranmu untuk memulai game!")
}

console.log("\n")
var nama3 = ["Lisa", "Noelle", "Tartaglia"]
var peran3 = "Penyihir"

if (peran3 == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[0])
    console.log("Halo Penyihir " + nama3[0] + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peran3 == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[0])
    console.log("Halo Guard " + nama3[0] + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (peran3 == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[0])
    console.log("Halo Werewolf " + nama3[0] + ", kamu akan memakan mangsa setiap malam!")
}

console.log("\n")
var nama3 = ["Lisa", "Noelle", "Tartaglia"]
var peran4 = "Guard"

if (peran4 == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[1])
    console.log("Halo Penyihir " + nama3[1] + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peran4 == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[1])
    console.log("Halo Guard " + nama3[1] + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (peran4 == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[1])
    console.log("Halo Werewolf " + nama3[1] + ", kamu akan memakan mangsa setiap malam!")
}

console.log("\n")
var nama3 = ["Lisa", "Noelle", "Tartaglia"]
var peran5 = "Werewolf"

if (peran5 == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[2])
    console.log("Halo Penyihir " + nama3[2] + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peran5 == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[2])
    console.log("Halo Guard " + nama3[2] + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (peran5 == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + nama3[2])
    console.log("Halo Werewolf " + nama3[2] + ", kamu akan memakan mangsa setiap malam!")
}


// 2) Switch Case
console.log("\n")
var tanggal = "17"
var bulan = 8
var tahun = "1945"
switch (bulan) {
    case 1: { (bulan = "Januari"); break; }
    case 2: { (bulan = "Februari"); break; }
    case 3: { (bulan = "Maret"); break; }
    case 4: { (bulan = "April"); break; }
    case 5: { (bulan = "Mei"); break; }
    case 6: { (bulan = "Juni"); break; }
    case 7: { (bulan = "Juli"); break; }
    case 8: { (bulan = "Agustus"); break; }
    case 9: { (bulan = "September"); break; }
    case 10: { (bulan = "Oktober"); break; }
    case 11: { (bulan = "November"); break; }
    case 12: { (bulan = "Desember"); break; }
    default: { ("Bulan"); }
} 

console.log(tanggal + " " + bulan + " " + tahun)
