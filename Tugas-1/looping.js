console.log("(1) While")
console.log('LOOPING PERTAMA')
var i = 0
while(i < 20) {
    i+=2
    console.log(i + " - I love coding")
}

console.log('\nLOOPING KEDUA')
var i = 20
while(i > 2) {
    i-=2
    console.log(i + " - I love coding")
}

console.log("\n(2) For")
for (var n=1; n<=20; n++) {
    if (n % 3 == 0 && n % 2 ==1) {
        console.log(n + " - I Love Coding")
    } else if (n % 2 == 1) {
        console.log(n + " - Teknik")
    } else if (n % 2 == 0) {
        console.log(n + " - Informatika")
    }
}

console.log("\n(3) Membuat Persegi Panjang #")
var x = 8
var y = 4
var num = 0
while (num < y){
    console.log("#".repeat(x))
    num++
}


console.log("\n(4) Membuat Tangga")
var numTangga = 7
for(var numA = 1; numA <= numTangga; numA++) {
    var blank = '';
    for(var numB = 1; numB <= numA; numB++) {
        var blank = blank + '#';
    }
    console.log(blank);
 }

console.log("\n(5) Membuat Papan Catur")

var caturX = 4
var caturY = 4
var caturNum = 0
while (caturNum < 4){
    console.log("# ".repeat(caturX))
    console.log(" #".repeat(caturY))
    caturNum++
}