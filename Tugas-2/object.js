//console.log("\n(1)")

personArr = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]

function arrayToObject(personArr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2021 (tahun sekarang)
    var jumlah = personArr.length
    
    for (i=0; i<jumlah; i++) {
        if (personArr[i][3] == undefined) {
            personArr[i][3] = "Invalid birth year"
        } else {
            var selisih = thisYear - personArr[i][3]
            if (selisih < 0) {
                personArr[i][3] = "Invalid birth year"
            } else {
                personArr[i][3] = selisih
            }
        }
        var nama = personArr[i][0] + " " + personArr[i][1]
        objek = {
            firstName : personArr[i][0],
            lastName : personArr[i][1],
            Gender : personArr [i][2],
            Age : personArr [i][3]
        }
        //  console.log(objek)
        hasil = (i+1) + ". " + nama + ": " + JSON.stringify(objek)
        console.log(hasil)
    }
}

//arrayToObject(personArr)

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)




// Error case
arrayToObject([]) // ""