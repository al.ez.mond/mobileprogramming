console.log("(1)")
function range (startNum, finishNum) {
    if (startNum == undefined || finishNum == undefined){
        return -1
    } else {
        var number = []
        if (startNum > finishNum) {
            while(startNum >= finishNum) {
                number.push(startNum)
                startNum--
            }
        } else {
            while(startNum <= finishNum) {
                number.push(startNum)
                startNum++
            }
        }
        return number
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


console.log("\n(2)")
function rangeWithStep (startNum, finishNum, step) {
    if (startNum == undefined || finishNum == undefined){
        return -1
    } else {
        var number = []
        var i = 0
        if (startNum > finishNum) {
            while(startNum >= finishNum) {
                number[i] = startNum
                startNum = startNum - step
                i++
            }
        } else {
            while(startNum <= finishNum) {
                number[i] = startNum
                startNum = startNum + step
                i++
            }
        }
        return number
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


console.log("\n(3)")
function sum(startNum, finishNum, step) {
    if (startNum == undefined) {
        jumlah = 0
    } else if (finishNum == undefined) {
        jumlah = startNum
    } else if (step == undefined) {
        var rangeSum = range(startNum, finishNum)
        var jumlah = 0
        for (i = 0; i<rangeSum.length; i++) {
            jumlah = jumlah + rangeSum[i]
        }      
    } else {
        var rangeSum = rangeWithStep(startNum, finishNum, step)
            var jumlah = 0
            for (i = 0; i<rangeSum.length; i++) {
                jumlah = jumlah + rangeSum[i]
            }
    }
    return jumlah
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log("\n(4)")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
]

function dataHandling() {
    j = 4
    data = []
    for (i=0; i<j; i++) {
            data[i] = "Nomor ID: " + input[i][0] + " \nNama Lengkap: " + input[i][1] + " \nTTL: " + input[i][2] + " " + input[i][3] + " \nHobi: " + input[i][4] + "\n"
    }
    return data
}

var output = dataHandling()
for (i=0; i<j; i++){
    console.log(output[i])
}


console.log("\n(5)")
function balikKata(str) {
    var newString = "";    
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }
    return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("Informatika")) // akitamrofnI
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Humanikers")) // srekinamuH ma I 